#include <stdio.h>
#include <math.h>

int isPalindromic(int n) {
	char str[6];
	int i, last = 6;

	sprintf(str, "%d", n);

	while(str[last] == 0)
		last--;

	for (i = 0; i <= (last / 2); i++)
		if (str[i] != str[last - i])
			return 0;

	return 1;
}

int main() {
	int i, j, product, max = 0;

	for (i = 0; i < 1000; i++)
		for (j = 0; j < 1000; j++) {
			product = i * j;
			if (isPalindromic(product) && product > max)
				max = product;
		}

	printf("%d\n", max);

	return 0;
}
