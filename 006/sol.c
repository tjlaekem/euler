#include <stdio.h>

int main() {
	int i, sum = 0, sum_of_squares = 0, square_of_sum = 0;

	for (i = 1; i <= 100; i++) {
		sum += i;
		sum_of_squares += i * i;
	}

	square_of_sum = sum * sum;

	printf("%d\n", square_of_sum - sum_of_squares);
}
