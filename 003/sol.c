#include <stdio.h>

long findLargestPrime(long numberToFactor) {
	long l;
	for (l = 2; l < numberToFactor; l++) {
		if (numberToFactor % l == 0)
			return findLargestPrime(numberToFactor / l);
	}
	return numberToFactor;
}

int main() {
	printf("%ld\n", findLargestPrime(600851475143));
	return 0;
}
