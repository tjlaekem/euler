#include <stdio.h>
#include <math.h>

int main() {
	int i = 2, j, prime_count = 0, prime;
	static int primes[10001];

	while (primes[10000] == 0) {
		prime = 1;
		for (j = 0; j < prime_count; j++)
			if (i % primes[j] == 0) {
				prime = 0;
				break;
			}
		if (prime) {
			primes[prime_count] = i;
			prime_count++;
		}
		i++;
	}

	printf("%d\n", primes[prime_count - 1]);
}
