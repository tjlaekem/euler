#include <stdio.h>

int* findPrimeFactors(int n, int* powers) {
	int i = n, j = 2;

	while (i >= j) {
		if (i % j == 0) {
			powers[j]++;
			i /= j;
			j = 1;
		}
		j++;
	}

	return powers;
}

int main() {
	int i, j, multiple = 1;
	static int powers[20];
	static int factors[20];

	for (i = 2; i < 21; i++) {
		findPrimeFactors(i, factors);
		for (j = 2; j < 21; j++) {
			powers[j] = (powers[j] > factors[j]) ? powers[j] : factors[j];
			factors[j] = 0;
		}
	}

	for (i = 1; i < 21; i++)
		for (j = 0; j < powers[i]; j++)
			multiple *= i;

	printf("%d\n", multiple);
}
