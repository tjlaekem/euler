#Smallest multiple

Find the least common multiple of the integers 1 to 20.

By finding the prime factorizations of each number between 1 and 20, the least common multiple can be found.  A number that is a multiple of 2^3 will also be a multiple of 2^2, meaning that maximum powers of every prime that occurs in any of the numbers can be multiplied in order to find the least common multiple.
